FROM anapsix/alpine-java

COPY target/NetCrackerTest-1.0-SNAPSHOT.jar /home/NetCrackerTest-1.0-SNAPSHOT.jar
RUN apk add dos2unix
COPY run.sh /usr/bin/run.sh
RUN chmod +x /usr/bin/run.sh
RUN dos2unix /usr/bin/run.sh

ENTRYPOINT ["/usr/bin/run.sh"]

#ENTRYPOINT ["java","-jar","/home/NetCrackerTest-1.0-SNAPSHOT.jar"]


