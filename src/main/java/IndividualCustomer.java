

public class IndividualCustomer implements Customer{
    private String name;
    private int age;
    private Address address;
    private TypeOf type = TypeOf.INDIVIDUAL;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public Address getAddress() {
        return address;
    }
    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "NewCustomer{" +
                "Имя = " + name +
                ", возраст = " + age +
                ", адрес = " + getAddress() +
                "\nтип заказчика = " + type +
                '}';
    }


}
