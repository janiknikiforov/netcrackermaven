

public class EntityCustomer implements Customer{
    private String name;
    private Address address;
    private TypeOf type = TypeOf.ENTITY;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Address getAddress() {
        return address;
    }
    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "NewCustomer{" +
                "Имя = " + name +
                ", адрес = " + getAddress() +
                "\nтип заказчика = " + type +
                '}';
    }


}